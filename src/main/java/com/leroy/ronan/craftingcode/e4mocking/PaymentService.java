package com.leroy.ronan.craftingcode.e4mocking;

public class PaymentService {
	
	private UserService userService;
	private PaymentGateway paymentGateway;

	protected PaymentService(UserService userService, PaymentGateway paymentGateway) {
		this.userService = userService;
		this.paymentGateway = paymentGateway;
	}

	public void processPayment(User user, PaymentDetails paymentDetails) {
		if (!userService.hasValidAccount(user)) {
			throw new InvalidUserAccountException();
		}
		paymentGateway.payWith(paymentDetails);
	}
}