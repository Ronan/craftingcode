package com.leroy.ronan.craftingcode.e1stack;

import java.util.ArrayList;
import java.util.List;

public class Stack {
	
	private List<Object> content = new ArrayList<>();
	
	public Object pop() throws EmptyStackException {
		if (content.isEmpty()) {
			throw new EmptyStackException();
		}
		Object result = content.get(content.size()-1);
		content.remove(content.size()-1);
		return result;
	}

	public void push(Object object) {
		this.content.add(object);
	}

}
