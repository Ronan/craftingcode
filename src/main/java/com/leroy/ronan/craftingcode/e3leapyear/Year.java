package com.leroy.ronan.craftingcode.e3leapyear;

public class Year {

	public static Year of(int year) {
		return new Year(year);
	}
	
	private int year;
	
	private Year(int year) {
		this.year = year;
	}

	public boolean isLeap() {
		if (this.isDivisibleBy(400)) {
			return true;
		} else if (this.isDivisibleBy(100) && !this.isDivisibleBy(400)) {
			return false;
		} else if (this.isDivisibleBy(4)) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isDivisibleBy(int i) {
		return this.year % i == 0;
	}
}
