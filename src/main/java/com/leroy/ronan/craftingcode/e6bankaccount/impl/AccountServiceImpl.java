package com.leroy.ronan.craftingcode.e6bankaccount.impl;

import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;

import com.leroy.ronan.craftingcode.e6bankaccount.AccountConsole;
import com.leroy.ronan.craftingcode.e6bankaccount.AccountService;
import com.leroy.ronan.craftingcode.e6bankaccount.StorageService;
import com.leroy.ronan.craftingcode.e6bankaccount.Transaction;

public class AccountServiceImpl implements AccountService{

	private StorageService storageService;
	private AccountConsole accountConsole;
	private DecimalFormat amountFormatter;
	
	public AccountServiceImpl(StorageService storageService, AccountConsole accountConsole) {
		super();
		this.storageService = storageService;
		this.accountConsole = accountConsole;
		this.amountFormatter = new DecimalFormat("###.00");
	}

	@Override
	public void deposit(int amount) {
		storageService.addTransaction(amount);
	}

	@Override
	public void withdraw(int amount) {
		storageService.addTransaction(amount * -1);
	}

	@Override
	public void printStatement() {
		accountConsole.printLine("DATE | AMOUNT | BALANCE");
		int balance = 0;
		for (Transaction transaction : storageService.getTransactions()) {
			balance += transaction.getAmount();
			String line = transaction.getDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) 
			    + " | " + amountFormatter.format(transaction.getAmount())
			    + " | " + amountFormatter.format(balance);
			accountConsole.printLine(line);
		}
	}
}
