package com.leroy.ronan.craftingcode.e6bankaccount;

import java.time.LocalDate;

public class Transaction {

	private LocalDate date;
	private int amount;
	
	public static Transaction of(LocalDate date, int amount) {
		return new Transaction(date, amount);
	}
	
	public LocalDate getDate() {
		return date;
	}

	public int getAmount() {
		return amount;
	}

	private Transaction(LocalDate date, int amount) {
		super();
		this.date = date;
		this.amount = amount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + amount;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (amount != other.amount)
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		return true;
	}
}
