package com.leroy.ronan.craftingcode.e6bankaccount;

public interface AccountService {

	public void deposit(int amount);

	public void withdraw(int amount);

	public void printStatement();
}
