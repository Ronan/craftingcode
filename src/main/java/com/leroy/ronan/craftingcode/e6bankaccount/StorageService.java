package com.leroy.ronan.craftingcode.e6bankaccount;

import java.util.List;

public interface StorageService {

	void addTransaction(int amount);

	List<Transaction> getTransactions();

}
