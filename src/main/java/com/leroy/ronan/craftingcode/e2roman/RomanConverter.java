package com.leroy.ronan.craftingcode.e2roman;

import java.util.HashMap;
import java.util.Map;

public class RomanConverter {

	private static final String Roman_L = "L";
	private static final String Roman_X = "X";
	private static final String Roman_V = "V";
	private static final String Roman_I = "I";
	

	public String convert(int arabicNumber) {
	
		for (RomanLetter romanLetter : RomanLetter.values()) {
			if (arabicNumber >= romanLetter.getArabic()) {
				return romanLetter.getRoman() 
						+ convert(arabicNumber - romanLetter.getArabic());
			}
		}

		String roman = "";
		while (arabicNumber > 0) {
			roman += Roman_I;
			arabicNumber--;
		}
		return roman;
	}
	
	
	private enum RomanLetter {
		ROMAN_L("L", 50),
		ROMAN_X("X", 10),
		ROMAN_V("V", 5),
		ROMAN_I("I", 1),
		;
		
		private String roman;
		private int arabic;
		private RomanLetter(String roman, int arabic) {
			this.roman = roman;
			this.arabic = arabic;
		}
		public String getRoman() {
			return roman;
		}
		public int getArabic() {
			return arabic;
		}
	}

}
