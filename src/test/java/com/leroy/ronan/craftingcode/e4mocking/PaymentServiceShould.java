package com.leroy.ronan.craftingcode.e4mocking;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class PaymentServiceShould {

	private UserService userService = Mockito.mock(UserService.class);
	private PaymentGateway paymentGateway = Mockito.mock(PaymentGateway.class);
	
	private PaymentService paymentService 
							= new PaymentService(userService, paymentGateway);
	
	private User user = Mockito.mock(User.class);
	private PaymentDetails paymentDetails = Mockito.mock(PaymentDetails.class);
	
	@Test(expected=InvalidUserAccountException.class)
	public void throw_exception_when_user_is_not_valid() {
		Mockito.when(userService.hasValidAccount(user)).thenReturn(false);
		
		paymentService.processPayment(user, paymentDetails);
	}

	@Test
	public void not_invoke_gateway_when_user_is_not_valid() {
		Mockito.when(userService.hasValidAccount(user)).thenReturn(false);
		
		try {
			paymentService.processPayment(user, paymentDetails);
			Assert.fail("InvalidUserAccountException should have been thrown");
		}catch (InvalidUserAccountException e) {
			
		}
		
		Mockito.verifyZeroInteractions(paymentGateway);
	}

	@Test
	public void invoke_gateway_when_user_is_valid() {
		Mockito.when(userService.hasValidAccount(user)).thenReturn(true);
		
		try {
			paymentService.processPayment(user, paymentDetails);
		}catch (InvalidUserAccountException e) {
			Assert.fail("InvalidUserAccountException should not have been thrown");
		}
		
		Mockito.verify(paymentGateway).payWith(paymentDetails);
	}
}
