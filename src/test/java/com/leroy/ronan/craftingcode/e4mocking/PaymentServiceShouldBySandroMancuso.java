package com.leroy.ronan.craftingcode.e4mocking;

import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PaymentServiceShouldBySandroMancuso {

	private static final User USER = new User();
	private static final User INVALID_USER = new User();
	private static final PaymentDetails PAYMENT_DETAILS = new PaymentDetails();

	@Mock UserService userService;
	@Mock PaymentGateway paymentGateway;

	private PaymentService paymentService;

	@Before
	public void initialise() {
		paymentService = new PaymentService(userService, paymentGateway);
	}

	@Test public void
	throw_exception_when_user_is_invalid() {
		given(userService.hasValidAccount(INVALID_USER)).willReturn(false);

		try {
			paymentService.processPayment(INVALID_USER, PAYMENT_DETAILS);
			fail("User service should have been called with INVALID_USER");
		} catch (InvalidUserAccountException e) {
			verify(userService).hasValidAccount(INVALID_USER);
		}

	}

	@Test public void
	not_invoke_payment_gateway_when_user_account_is_invalid() {
		given(userService.hasValidAccount(INVALID_USER)).willReturn(false);

		try {
			paymentService.processPayment(INVALID_USER, PAYMENT_DETAILS);
			fail("InvalidUserAccountException should have been thrown");
		} catch (InvalidUserAccountException e) {
			verifyZeroInteractions(paymentGateway);
		}
	}

	@Test public void
	process_payment_details_when_user_is_valid() {
		given(userService.hasValidAccount(USER)).willReturn(true);

		paymentService.processPayment(USER, PAYMENT_DETAILS);

		verify(paymentGateway).payWith(PAYMENT_DETAILS);
	}

}
