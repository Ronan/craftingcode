package com.leroy.ronan.craftingcode.e1stack;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import com.leroy.ronan.craftingcode.e1stack.EmptyStackException;
import com.leroy.ronan.craftingcode.e1stack.Stack;


public class MyStackShould {
	
	@Test(expected=EmptyStackException.class)
	public void throw_exception_when_pop_on_an_empty_stack() throws Exception{
		Stack stack = new Stack();

		stack.pop();
	}

	@Test
	public void pop_the_last_pushed_object_after_1_push() throws Exception{
		Object object = new Object();
		Stack stack = new Stack();
		
		stack.push(object);
		
		assertEquals(object, stack.pop());
	}
	
	@Test
	public void pop_the_last_pushed_object_after_2_pushes() throws Exception{
		Object firstObject = new Object();
		Object secondObject = new Object();
		Stack stack = new Stack();
		
		stack.push(firstObject);
		stack.push(secondObject);
		
		assertEquals(secondObject, stack.pop());
	}
	
	@Test
	public void pop_the_first_pushed_object_after_2_pushes_and_1_pop() throws Exception{
		Object firstObject = new Object();
		Object secondObject = new Object();
		Stack stack = new Stack();
		
		stack.push(firstObject);
		stack.push(secondObject);
		stack.pop();
		
		assertEquals(firstObject, stack.pop());
	}
	
	@Test(expected=EmptyStackException.class)
	public void throw_exception_when_more_pops_than_pushes() throws Exception{
		Stack stack = new Stack();
		Object object = new Object();
		
		stack.push(object);
		stack.pop();
		stack.pop();
	}
	
	@Test
	public void pop_null_when_null_was_pushed() throws Exception {
		Stack stack = new Stack();

		stack.push(null);
		
		assertNull(stack.pop());
	}
}
