package com.leroy.ronan.craftingcode.e3leapyear;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.leroy.ronan.craftingcode.e3leapyear.Year;

public class LeapYearShould {

	@Test
	public void leap_year_tests() {
		
		assertThat(Year.of(2016).isLeap(), is(true));
		assertThat(Year.of(1900).isLeap(), is(false));
		assertThat(Year.of(2000).isLeap(), is(true));

	}
	
}
