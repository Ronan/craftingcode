package com.leroy.ronan.craftingcode.e2roman;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import org.junit.Test;

import com.leroy.ronan.craftingcode.e2roman.RomanConverter;

public class RomanConverterShould {

	RomanConverter converter = new RomanConverter();

	@Test
	public void convert_1_to_I(){
		assertThat(converter.convert(1), is("I"));
	}

	@Test
	public void convert_2_to_II() throws Exception {
		assertThat(converter.convert(2), is("II"));
	}
	
	@Test
	public void convert_3_to_III() throws Exception {
		assertThat(converter.convert(3), is("III"));
	}
	
	@Test
	public void convert_5_to_V() throws Exception {
		assertThat(converter.convert(5), is("V"));
	}
	
	@Test
	public void convert_6_to_VI() throws Exception {
		assertThat(converter.convert(6), is("VI"));
	}

	@Test
	public void convert_10_to_X() throws Exception {
		assertThat(converter.convert(10), is("X"));
	}

	@Test
	public void convert_17_to_XVII() throws Exception {
		assertThat(converter.convert(17), is("XVII"));
	}
	
	@Test
	public void convert_78_to_LXXVIII() throws Exception {
		assertThat(converter.convert(78), is("LXXVIII"));
	}

}
