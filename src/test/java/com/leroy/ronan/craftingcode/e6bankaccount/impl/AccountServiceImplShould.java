package com.leroy.ronan.craftingcode.e6bankaccount.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.leroy.ronan.craftingcode.e6bankaccount.AccountConsole;
import com.leroy.ronan.craftingcode.e6bankaccount.AccountService;
import com.leroy.ronan.craftingcode.e6bankaccount.StorageService;
import com.leroy.ronan.craftingcode.e6bankaccount.Transaction;

public class AccountServiceImplShould {

	private StorageService storageService = Mockito.mock(StorageService.class);
	private AccountConsole accountConsole = Mockito.mock(AccountConsole.class);

	private AccountService service;
	
	@Before
	public void before() {
		service = new AccountServiceImpl(storageService, accountConsole);
	}
	
	@Test
	public void add_a_transaction_when_I_deposit() {
		
		service.deposit(100);
		
		Mockito.verify(storageService).addTransaction(100);
	}
	
	@Test
	public void add_a_transaction_when_I_withdraw() {
		
		service.withdraw(100);
		
		Mockito.verify(storageService).addTransaction(-100);
	}
	
	@Test
	public void print_empty_statement_with_only_header() {
		service.printStatement();
		
		Mockito.verify(accountConsole).printLine("DATE | AMOUNT | BALANCE");
	}
	
	@Test
	public void print_statement_with_one_deposit() {
		List<Transaction> transactions = new ArrayList<>();
		transactions.add(Transaction.of(LocalDate.of(2014, 4, 2), 200));
		Mockito.when(storageService.getTransactions()).thenReturn(transactions);
		
		service.printStatement();
		
		Mockito.verify(accountConsole).printLine("DATE | AMOUNT | BALANCE");
		Mockito.verify(accountConsole).printLine("02/04/2014 | 200.00 | 200.00");
	}

	@Test
	public void print_statement_with_several_deposits_and_withdraws() {
		List<Transaction> transactions = new ArrayList<>();
		transactions.add(Transaction.of(LocalDate.of(2014, 4, 2), 1000));
		transactions.add(Transaction.of(LocalDate.of(2014, 4, 10), -500));
		transactions.add(Transaction.of(LocalDate.of(2014, 4, 1), 200));
		Mockito.when(storageService.getTransactions()).thenReturn(transactions);
		
		service.printStatement();
		
		Mockito.verify(accountConsole).printLine("DATE | AMOUNT | BALANCE");
		Mockito.verify(accountConsole).printLine("10/04/2014 | -500.00 | 700.00");
		Mockito.verify(accountConsole).printLine("02/04/2014 | 1000.00 | 1200.00");
		Mockito.verify(accountConsole).printLine("01/04/2014 | 200.00 | 200.00");
	}

}
