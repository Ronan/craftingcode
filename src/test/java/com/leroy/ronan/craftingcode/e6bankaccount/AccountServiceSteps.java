package com.leroy.ronan.craftingcode.e6bankaccount;

import org.mockito.Mockito;

import com.leroy.ronan.craftingcode.e6bankaccount.impl.AccountServiceImpl;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AccountServiceSteps {

	private AccountService accountService;
	private int DEPOSIT_AMOUNT = 500;
	private AccountConsole console = Mockito.mock(AccountConsole.class);
	private StorageService storageService = Mockito.mock(StorageService.class);

	@Given("^an Account$")
	public void an_Account() throws Throwable {
	    accountService = new AccountServiceImpl(storageService, console);
	}

	@When("^I deposit into Account$")
	public void i_deposit_into_Account() throws Throwable {
	    accountService.deposit(DEPOSIT_AMOUNT);
	}

	@When("^I deposit (\\d+)$")
	public void i_deposit(int amount) throws Throwable {
	    accountService.deposit(amount);
	}

	@When("^I withdraw (\\d+)$")
	public void i_withdraw(int amount) throws Throwable {
	    accountService.withdraw(amount);
	}

	@When("^I print the bank statement$")
	public void i_print_the_bank_statement() throws Throwable {
		accountService.printStatement();
	}

	@Then("^the deposit should be stored$")
	public void the_deposit_should_be_stored() throws Throwable {
		throw new PendingException("I don't know what I am doing !");
	}

	@Then("^the following statement should be produced :$")
	public void the_following_statement_should_be_produced(String output) throws Throwable {
		throw new PendingException();
	}

	@Then("^the line \"([^\"]*)\" should be printed in the console$")
	public void the_line_should_be_printed_in_the_console(String line) throws Throwable {
		Mockito.verify(console).printLine(line);
	}

	
}
