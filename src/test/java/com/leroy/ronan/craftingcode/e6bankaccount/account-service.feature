Feature: Features of Account Service

Objective:
Learn and practice the double loop of TDD
Test application from outside, according to side effect
  
Problem description:  Bank kata
  
Create a simple bank application with the following features:
  
- Deposit into Account
- Withdraw from an Account
- Print a bank statement to the console.
  
Acceptance criteria
Statement should have the following the format:
    DATE       | AMOUNT  | BALANCE
  10/04/2014 | 500.00  | 1400.00
  02/04/2014 | -100.00 | 900.00
  01/04/2014 | 1000.00 | 1000.00
 
Note: Start with an acceptance test

Scenario: Deposit
Given an Account
 When I deposit into Account
 Then the deposit should be stored

Scenario: Acceptance
Given an Account
 When I deposit 1000
  And I withdraw 100
  And I deposit 500
  And I print the bank statement
 Then the line "DATE       | AMOUNT  | BALANCE" should be printed in the console
 Then the line "27/04/2016 | 1000.00 | 1000.00" should be printed in the console
 Then the line "27/04/2016 | -100.00 | 900.00" should be printed in the console
 Then the line "27/04/2016 |  500.00 | 1400.00" should be printed in the console
