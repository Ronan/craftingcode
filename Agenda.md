# Agenda

## Day 1

- Introduction to TDD (Failing Test)
- TDD Lifecycle and Naming (Stack)
- Test-Driving algorithms (Roman Numerals)
- Expressing business rules (Leap Year)
- Mocking (PaymentService)

## Day 2

- Expressive Tests with Builders
- Outside-In TDD with Acceptance Tests
- Testing and Refactoring Legacy Code