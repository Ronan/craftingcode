# Crafting Code

@sandromancuso

sandro@codurance.com

http://codingboard.org/boards/craftingcode

## E.1 – TDD lifecycle & naming 

Objective
- Introduce naming convention
- Create production code from test
- Start from assertion
- Tip for deciding the first test to write: The simplest possible.

Problem description: Stack

Implement a Stack class with the following public methods:

+ void push(Object object)
+ Object pop()

Stack should throw an exception if popped when empty.

## E.2 – Test-Driving Algorithms
  
Objective
- Grow an algorithm bit by bit
- Delay treating exceptions (in this case, because they are more complex)
- Intentionally cause duplication
- Focus on simple structures first
 
Problem description: Roman Numerals Converter
  
Implement a Roman numeral converter. The code must be able to take decimals up
to 3999 and convert to their roman equivalent.
 
Examples
1    - I
5    - V
10   - X
50   - L
100  - C
500  - D
1000 - M
2499 - MMCDXCIX
3949 - MMMCMXLIX

=> Discipline

- ({}–>nil) no code at all->code that employs nil
- (nil->constant)
- (constant->constant+) a simple constant to a more complex constant
- (constant->scalar) replacing a constant with a variable or an argument
- (statement->statements) adding more unconditional statements.
- (unconditional->if) splitting the execution path
- (scalar->array)
- (array->container)
- (statement->recursion)
- (if->while)
- (expression->function) replacing an expression with a function or algorithm
- (variable->assignment) replacing the value of a variable.

## E.3 - Expressing Business Rules

Objective
- Clearly express business rules and domain.
- It’s OK to write a passing test if it express a valid business rule.
 
Problem description: Leap Year
 
All the following rules must be satisfied:
- Is leap year if divisible by 400
- Is NOT leap year if divisible by 100 but not by 400
- Is leap year is divisible by 4

Express domain
Service class : LeapYearValidator.validate()
LeapYear.isLeapYear() => Cannot response false !
Year.isLeap(2016) / new Year().isLeap(2016) => This is service class.

new Year(2016).isLeap() => Behavior attractor.

## E.4 – Mocking
 
Problem description:  Payment service


Given a user wants to buy her selected items
When she submits her payment details
Then we should process her payment

Acceptance criteria:
- If the user is not valid, an exception should be thrown.
- Payment should be sent to the payment gateway.

Create a class with the following signature:

```java
public class PaymentService {
    public void processPayment(User user,
                               PaymentDetails paymentDetails) {
        // your code goes here
    }
}
```

Outside IN : Other style of TDD, using mock.

Validate you know how to use your mocking framework to :

- Mock objects.
- Mock behavior of mocked objects.
- Check method of mocked objects were called.

## E.6 – Outside-In TDD with acceptance test

### Bank account

Double loop of testing (Unit Test / Acceptance Test)

Unit Test : Little and simple, use mocks.

- 1 method

Acceptance Test : Big, whole system as a black box.

- A whole scenario

Integration Test : In the boundary of the system.

Reduce interactions.
All decisions have drawbacks.
Be concious of your choices.

## Legacy Code

- Tight Coupling
- Low Cohesion
- Feature Envy
- Anaemic Domain
- Single Responsibility Principle violation
	- One reason to change (inside)
	- Do what it says (outside)

http://connascence.io/

## E.6 - trip-service-kata

https://github.com/sandromancuso/trip-service-kata

- Start testing from shallowest to deepest
- Start refactoring from deepest to shallowest
- SEAM : extract the call to a collaborator in a method.

https://marketplace.eclipse.org/content/atlassian-clover-eclipse

http://www.objectaid.com/

http://arlobelshee.com/good-naming-is-a-process-not-a-single-step/

http://www.karolikl.com/2015/10/what-is-actual-life-expectancy-of-your.html

